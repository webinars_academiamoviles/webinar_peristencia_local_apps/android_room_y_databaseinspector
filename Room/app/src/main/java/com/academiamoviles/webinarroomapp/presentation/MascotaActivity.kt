package com.academiamoviles.webinarroomapp.presentation

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.academiamoviles.webinarroomapp.R
import com.academiamoviles.webinarroomapp.data.AppDatabase
import com.academiamoviles.webinarroomapp.data.Mascota
import com.academiamoviles.webinarroomapp.databinding.ActivityMascotaBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MascotaActivity : AppCompatActivity() {

    private lateinit var binding : ActivityMascotaBinding
    private val appDatabase by lazy{
        AppDatabase.getInstance(this)
    }

    private val mascotaAdapter by lazy {
        MascotaAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMascotaBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
        setUpAdapter()
    }

    private fun init() {

        appDatabase?.mascotaDao()?.getAlls().observe(this, Observer {

            mascotaAdapter.updateData(it)
        })

        binding.fabAgregar.setOnClickListener {
            irRegistro()
        }
    }

    private fun irRegistro() {
        val intent = Intent(this,RegistroActivity::class.java)
        startActivity(intent)
    }

    private fun setUpAdapter() {

        binding.recyclerMascotas.adapter = mascotaAdapter
        binding.recyclerMascotas.layoutManager = LinearLayoutManager(this)
    }

}