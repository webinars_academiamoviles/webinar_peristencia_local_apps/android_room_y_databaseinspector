package com.academiamoviles.webinarroomapp.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface MascotaDao {

    @Insert
    suspend fun insert(mascotas:Mascota):Long

    @Query("select *from tablaMascota")
    fun getAlls() : LiveData<List<Mascota>>
}