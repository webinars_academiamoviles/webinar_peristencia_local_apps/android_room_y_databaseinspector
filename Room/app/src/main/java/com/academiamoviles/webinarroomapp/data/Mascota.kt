package com.academiamoviles.webinarroomapp.data

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tablaMascota")
data class Mascota(

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "codigo")
    val codigo:Int,

    @ColumnInfo(name = "nombres")
    val nombres:String,

    @ColumnInfo(name = "raza")
    val raza:String,

    @ColumnInfo(name = "preferencia")
    val preferencia:String) {
}